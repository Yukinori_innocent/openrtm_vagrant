# Vagrantfile and Chef cookbooks for OpenRTM #

openRTM-aist1.1.2の入ったubuntu14.04 vmを作成するためのVagrantfileとChefのcookbookです。
Vagrantを使うことで環境を構築するのが容易になります  
windowsで使うための方法を書いておきます  
すでに[Virtualbox](https://www.virtualbox.org/)がインストールされているものとして始めます  
## ホストOSに必要なもののインストール##
まずは下記urlからVagrant本体をDLしてインストール

[Vagrant](https://www.vagrantup.com/)

続いてChefを使うためにRuby(最新版)とDevKitをインストール(下記リンクに二つともあります)  
Development Kit はzipを展開するときにファイルが散乱してしまうのでまず空のディレクトリを作成してそこに展開してください  

[RubyInstaller & Development Kit](http://rubyinstaller.org/downloads/) 

最後にChefとBerkshelf(chef cookbookの依存関係マネージャー)を使うためにChefDKをインストールします。

[Chef Development Kit](https://downloads.chef.io/chef-dk/)

**Windowsの人はここで一旦再起動してください**
## Vagrant plugin のインストール##
###コマンドプロンプトを開き以下のコマンドを実行します###

```
vagrant plugin install vagrant-omnibus :chefをVMに自動でインストールするためのplugin
vagrant plugin install vagrant-berkshelf :vagrantでberkshelfを使うためのplugin
vagrant plugin install sahara :VMのロールバックを可能にするためのplugin
```


## リポジトリのクローン ##
git環境が入ってる方はこのリポジトリを任意の場所にクローンしてください

```
git clone https://bitbucket.org/Yukinori_innocent/openrtm_vagrant.git
```
入っていない方は下記ＵＲＬからダウンロードして任意の場所に展開してください

https://bitbucket.org/Yukinori_innocent/openrtm_vagrant/get/8cc7fa676e31.zip

## VMを立ち上げる##
VMを立ち上げる前にtext editorでVagrantfileを編集します  
VMのipアドレスとフォワーディングするポートを指定します  
naming serverのために2809番ポートもマッピングしておきます  
\# でコメントアウトされているので各人の環境に合わせて数値を設定したうえで外してください  
`ssh`のport等も適宜変更できます(defaultはhostの2222にフォワーディングされてるはず)。

```
#config.vm.network "forwarded_port", guest: 80, host: 8080
#config.vm.network "forwarded_port", guest: 2809,host: 12809
...
#config.vm.network "private_network", ip: "192.168.33.10"
```

VMを立ち上げるためには以下のコマンドをVagranfileがあるディレクトリでたたきます

```
 vagrant up
```
初回の`vagrant up`でchefがVMにインストールされopenRTMを含めた必要とされるソフトウェアがすべてインストールされmongodbとrtm-namingが立ち上がるはずです。  
何事もなければこれで環境構築は完了となります。

## Vagrantについて##
Vagrantはvirtualboxなどの仮想環境ツールと合わせて使うことにより各VMの設定や管理をより容易にするためのツールです。
Vagrantを使うことにより仮に仮想環境がおかしくなったりしてしまったとしても簡単に再構築だったりロールバックが可能になります
設定ファイルはVagrantfileになります。基本的にVMはVagrantfileが置いてあるディレクトリで操作します。  
基本的な操作は以下の通りです（その他はヘルプ等参照)
```
vagrant up //VMを立ち上げる
vagrant halt //VMを止める
vagrant resume //サスペンドされてる再開する
vagrant suspend //VMをサスペンドする
vagrant provision //立ち上がっているVMに対してchef等を適用する(止まっているVMはvagrant upだけで問題ない)
vagrant  destoroy //VMを破棄する

//以下今回はたたく必要はないが知っておくと便利
vagrant box add {任意の名前} {boxのURL} //boxと呼ばれるVM imageをDLする
vagrant init {任意の名前} // 指定したboxを用いてそのディレクトリにVagrantfileと.vagrantディレクトリを作成する

//VMにsshではいります、ただしopenSSHがホストOSに入ってる必要があります
vagrant ssh 
vagrant ssh-config //ssh関連情報の表示
```
Putty等任意のソフトウェアでsshしたい場合は`vagrant ssh-config` で表示された情報をもとに接続してください  
デフォルトユーザー名:vagrant 
デフォルトパスワード:vagrant
今回はVagrantfileにurlを指定してるため(ubuntu14.04)特に設定する必要はありませんが  
boxのURLを指定してDLすることにより簡単に任意のOSイメージをVMとして作成することができます。
boxは以下のサイトにまとめられています  
http://www.vagrantbox.es/

### sahara ###
今回インストールしたsahara pluginを使うことにより容易にVMのロールバックが可能になります
```
vagrant sandbox on //sandboxモード開始
vagrant sandbox commit //変更を決定する
vagrant sandbox rollback //変更を破棄してロールバックする
vagrant sandbox off //sandboxモード終了
vagrant sandbox status //現在sandboxモードかどうかの確認

```
VMに対して何かしら変更を行う場合にやっておくと環境がおかしくなった場合に簡単に基に戻せます

## Chefについて##
`chef`は自動でサーバーの環境を整えるためのprovisioning toolです。  
このリポジトリにおいては`site-cookbooks`以下に`cookbook`と呼ばれるディレクトリがいくつかあります  
各cookbookには`recipes`というディレクトリがありその中にあるrubyで書かれたスクリプト(etc. default.rb)が実際に実行されるものになります  
特に変更する必要はないと思いますが任意のcookbookを作成したい場合は以下のコマンドを実行し新しいcookbook作成してください  
```
//site-cookbooksに移動して
berks cookbook {cookbookの名前}
```
上記のコマンドでcookbookのひな形が生成されるのでその後`site-cookbooks/{cookbook名}/recipes/default.rb`を編集してください  
各cookbookに対してインストールされるソフトウェアは以下の通りです。またmongodbは自動的にサービスがenableかつstartするようになっています。  
rtm-namingもVMが立ち上がると同時に起動します  

* openrtm
  - default-jdk
  - cmake
  - doxygen
  - doxygen-gui
  - graphviz
  - libopencv-dev
  - python-opencv
  - openrtm

* python_utils 
  - python-numpy
  - python-scipy
  - python-matplotlib
  - libpython2.7-dev
  - libpython3.4-dev
  - python3-numpy
  - python3-scipy
  - pytho3-matplotlib


* mongodb
  - mogodb-service