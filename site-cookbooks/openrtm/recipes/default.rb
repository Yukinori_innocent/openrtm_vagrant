#
# Cookbook Name:: openrtm
# Recipe:: default
#
# Copyright (C) 2016 YOUR_NAME
#
# All rights reserved - Do Not Redistribute
#
%w{default-jdk cmake doxygen doxygen-gui graphviz libopencv-dev python-opencv}.each do |pkg|
 package pkg do
	action :install
 end
end 

execute "action" do
 command <<-EOH
	ln /dev/null /dev/raw1394
	wget "http://svn.openrtm.org/OpenRTM-aist/tags/RELEASE_1_1_2/OpenRTM-aist/build/pkg_install_ubuntu.sh"	
	sh pkg_install_ubuntu.sh -s 
	sh pkg_install_ubuntu.sh -d  
	sh pkg_install_ubuntu.sh -c  
	sh pkg_install_ubuntu.sh -r 
 EOH
end	

