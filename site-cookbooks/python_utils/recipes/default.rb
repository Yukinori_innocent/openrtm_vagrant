#
# Cookbook Name:: pyton_utils
# Recipe:: default
#
# Copyright (C) 2016 YOUR_NAME
#
# All rights reserved - Do Not Redistribute

%w{libpython3.4-dev python3-numpy python3-scipy python3-matplotlib python-dev build-essential python-pip}.each do |pkg|
	package pkg do
		action :install
	end
end
execute "action" do
 command <<-EOH
   pip install pymongo 
 EOH
end