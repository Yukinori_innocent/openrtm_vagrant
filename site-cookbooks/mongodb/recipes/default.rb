#
# Cookbook Name:: mongodb
# Recipe:: default
#
# Copyright (C) 2016 YOUR_NAME
#
# All rights reserved - Do Not Redistribute
#
package "mongodb-server" do
	action :install
end

service "mongodb" do
	action [:enable,:start]
end
